from django.conf.urls import url
from django.urls import re_path, include
from rest_framework import routers

from app_api.api.views import ArtistViewSet, AlbumViewSet, SongViewSet

router = routers.DefaultRouter()
router.register(r'artista', ArtistViewSet, basename='artista')
router.register(r'album', AlbumViewSet, basename='album')
router.register(r'cancion', SongViewSet, basename='cancion')

urlpatterns = router.urls

# urlpatterns = [
#     url(r'^', include(router.urls)),
#     re_path('^artista/(?P<username>.+)/$', ArtistViewSet.as_view({'get': 'list'})),
# ]
