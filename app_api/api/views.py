from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated

from app_api.api.serializers import ArtistSerializer, AlbumSerializer, SongSerializer
from app_api.models.album import Album, Song
from app_api.models.artistas import Artist


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10000


class AbstractFilterView(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    pagination_class = LargeResultsSetPagination
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]


class ArtistViewSet(AbstractFilterView):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer
    filterset_fields = ['name']


class AlbumViewSet(AbstractFilterView):
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer
    filterset_fields = ['name']


class SongViewSet(AbstractFilterView):
    queryset = Song.objects.all()
    serializer_class = SongSerializer
    filterset_fields = ['name', 'album__name', 'album__artist__name']
