from rest_framework import serializers

from app_api.models.album import Album, Song
from app_api.models.artistas import Artist
from app_api.models.ubigeo import Nationality


class NationalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Nationality
        fields = ['id', 'name']


class ArtistSerializer(serializers.ModelSerializer):
    nationality_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Artist
        fields = ['id', 'name', 'description', 'nationality', 'nationality_id']
        depth = 2


class AlbumSerializer(serializers.ModelSerializer):
    artist_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Album
        fields = ['id', 'name', 'img', 'description', 'release_ayer', 'artist', 'artist_id', 'total_song',
                  'total_minutes']
        depth = 2


class SongSerializer(serializers.ModelSerializer):
    album_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Song
        fields = ['id', 'name', 'duration', 'album', 'album_id']
        depth = 3

    def validate_name(self, value):
        if len(Song.objects.filter(name=value, album_id=int(self.initial_data['album_id']))) == 0:
            return value
        raise serializers.ValidationError("Ya existe una cancion para este artista")
