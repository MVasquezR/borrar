from django.db import models
from django.utils.translation import gettext as _


class Nationality(models.Model):
    class Meta:
        verbose_name = _('Nacionalidad')
        verbose_name_plural = _('Nacionalidades')

    name = models.CharField(blank=False, null=False, max_length=256, verbose_name='Nombre')

    def __str__(self):
        return '{}'.format(self.name)
