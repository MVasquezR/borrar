from django.db import models
from django.utils.translation import gettext as _

from app_api.models.ubigeo import Nationality


class Artist(models.Model):
    class Meta:
        verbose_name = _('Artista')
        verbose_name_plural = _('Artistas')

    name = models.CharField(blank=False, null=False, max_length=256, verbose_name='Nombre', unique=True)
    description = models.TextField(blank=False, null=False, verbose_name='Acerca de')
    nationality = models.ForeignKey(Nationality, on_delete=models.PROTECT, verbose_name='Nacionalidad')

    def __str__(self):
        return '{}'.format(self.name)
