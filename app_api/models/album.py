import os
import uuid

from django.db import models
from django.db.models import Sum
from django.utils.translation import gettext as _

from app_api.models.artistas import Artist


class Album(models.Model):
    class Meta:
        verbose_name = _('Album')
        verbose_name_plural = _('Albunes')

    def image_path(instance, filename):
        filename, file_extension = os.path.splitext(filename)
        ext = filename.split('.')[-1]
        name = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join(name + file_extension)

    name = models.CharField(blank=False, null=False, max_length=256, verbose_name='Nombre', unique=True)
    img = models.ImageField(max_length=1000, upload_to=image_path, null=False, blank=False, verbose_name='Portada')
    description = models.TextField(blank=False, null=False, verbose_name='Descripcion del album')
    release_ayer = models.IntegerField(null=False, blank=False, verbose_name='Año de lanzamiento')
    artist = models.ForeignKey(Artist, on_delete=models.PROTECT, verbose_name='Artista')

    @property
    def total_song(self):
        return len(Song.objects.filter(album_id=self.id))

    @property
    def total_minutes(self):
        from datetime import timedelta
        delta = None
        for index, s in enumerate(Song.objects.filter(album_id=self.id)):
            if index == 0:
                delta = timedelta(hours=s.duration.hour, minutes=s.duration.minute, seconds=s.duration.second)
            else:
                delta = delta + timedelta(hours=s.duration.hour, minutes=s.duration.minute, seconds=s.duration.second)
        return str(delta)

    def __str__(self):
        return '{}'.format(self.name)


class Song(models.Model):
    class Meta:
        verbose_name = _('Cancion')
        verbose_name_plural = _('Canciones')

    name = models.CharField(blank=False, null=False, max_length=256, verbose_name='Nombre')
    duration = models.TimeField(blank=False, null=False, verbose_name='Duración')
    album = models.ForeignKey(Album, null=False, blank=False, verbose_name='Album', on_delete=models.PROTECT)

    def __str__(self):
        return '{}'.format(self.name)
