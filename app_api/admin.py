from django.contrib import admin

from app_api.models.ubigeo import Nationality


@admin.register(Nationality)
class RegistryTaskAutomationAdmin(admin.ModelAdmin):
    list_display = ('name',)
