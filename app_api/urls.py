from django.conf.urls import url
from django.urls import include
from rest_framework import routers

router = routers.DefaultRouter()

app_name = 'app_website'

urlpatterns = [
    url('api/', include(('app_api.api.urls', 'api'), namespace='api')),
]

urlpatterns += router.urls
